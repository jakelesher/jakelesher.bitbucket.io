var searchData=
[
  ['sawwave_0',['SawWave',['../_blinking_l_e_d_8py.html#adcad1f7523001d31da28dcbff646e5cb',1,'BlinkingLED']]],
  ['scan_1',['Scan',['../classtouchpanel_1_1_touch_panel.html#a67537f8164e31eb37e2311437fea2d2c',1,'touchpanel::TouchPanel']]],
  ['set_5fduty_2',['set_duty',['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)'],['../classmotor_1_1_motor.html#a08f41a32c8b122a8b94496fef2a0a901',1,'motor.Motor.set_duty(self, duty)']]],
  ['set_5fgain_3',['set_gain',['../class_closed_loop_1_1_closed_loop.html#a01a3398bfd1a6ac43d8084b2435dc386',1,'ClosedLoop.ClosedLoop.set_gain(self, Kp, KI, omega_ref)'],['../class_closed_loop_1_1_closed_loop.html#a131eb47bd8b49a4ff2fb4757bc39ffc6',1,'ClosedLoop.ClosedLoop.set_gain(self, Kp, KI, Kd)']]],
  ['set_5fgain_5finner_4',['set_gain_inner',['../class_closed_loop_1_1_closed_loop.html#a6e228035c78c2e3061352509b9d939e4',1,'ClosedLoop::ClosedLoop']]],
  ['set_5fgain_5fouter_5',['set_gain_outer',['../class_closed_loop_1_1_closed_loop.html#a877d1469bd7e2c7c7d6997330df8c87c',1,'ClosedLoop::ClosedLoop']]],
  ['sinewave_6',['SineWave',['../_blinking_l_e_d_8py.html#a6239189578fc1a14d1d3ee750ab53973',1,'BlinkingLED']]],
  ['squarewave_7',['SquareWave',['../_blinking_l_e_d_8py.html#aa13f8660e5b1689d36810f2507421a81',1,'BlinkingLED']]],
  ['status_8',['status',['../class_b_n_o055_1_1_b_n_o055.html#a0433272a8840f85f1fd216e733312a85',1,'BNO055.BNO055.status(self)'],['../class_b_n_o055_1_1_b_n_o055.html#a0433272a8840f85f1fd216e733312a85',1,'BNO055.BNO055.status(self)']]]
];
